<?php

Route::group([
    'midleware' =>'api',
    'prefix' =>'auth',
    'namespace' => 'Auth'
    ],function ($router) {
        Route::post('register','RegisterController@store');
        Route::post('verification','RegisterController@verification');
        Route::post('regenerate-otp','RegisterController@regenerateOtp');
        Route::post('update-password','RegisterController@updatePassword');
        Route::post('login','LoginController@login');
        Route::post('logout','LogoutController')->middleware('auth:api');
        Route::post('check-token','CheckTokenController')->middleware('auth:api');
        Route::get('/social/{provider}','SocialController@redirectToProvider');
        Route::get('/social/{provider}/callback','SocialController@handleProvidercallback');
});

Route::group([
    'namespace' => 'API',
    'middleware' => 'auth:api'
    ], function($router){
        Route::get('get-profile','UserController@getProfile');
        Route::post('update-profile','UserController@updateProfile');
});

Route::group([
    'namespace' => 'API',
    'middleware' => 'api',
    'prefix' => 'campaign',
    ], function(){
    Route::get('random/{count}','CampaignController@random');
    Route::post('store','CampaignController@store');
    Route::get('/','CampaignController@index');
    Route::get('/{id}','CampaignController@detail');
    Route::get('/search/{keyword}','CampaignController@search');
});

Route::group([
    'namespace' => 'API',
    'middleware' => 'api',
    'prefix' => 'blog',
    ], function(){
    Route::get('random/{count}','BlogController@random');
    Route::post('store','BlogController@store');
});
