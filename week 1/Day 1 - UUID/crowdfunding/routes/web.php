<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('{any?}','app')->where('any','.*');

// Route::get('test-1','TestController@test1')->middleware('emailVerified');
// Route::get('test-2','TestController@test2')->middleware(['emailVerified','admin']);

// Route::get('tugas-vue','TugasVueController');
// Route::view('/quiz-2','quiz2');

// Route::get('/home', 'HomeController@index')->name('home');