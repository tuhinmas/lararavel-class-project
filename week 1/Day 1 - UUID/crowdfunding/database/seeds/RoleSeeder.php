<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = [
            ["role_name" => "admin"],
            ["role_name" => "user"]
        ];
        foreach($role as $roles){
            Role::create($roles);
        }
    }
}
