<?php

namespace App\Listeners;

use App\Models\OTP;
use App\Events\RegisterOtp;
use Illuminate\Support\Facades\Mail;
use App\Mail\GenerateOtpUserRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailNotificationUserRegister implements ShouldQueue
{
    public $otp;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  RegisterOtp  $event
     * @return void
     */
    public function handle(RegisterOtp $event)
    {
        Mail::to($event->otp->users)->send(new GenerateOtpUserRegistered($event->otp));
    }
}
