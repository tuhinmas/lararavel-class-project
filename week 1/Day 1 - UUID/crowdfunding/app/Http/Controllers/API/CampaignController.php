<?php

namespace App\Http\Controllers\APi;

use App\Http\Controllers\Controller;
use App\Models\Campaign;
use Illuminate\Http\Request;

class CampaignController extends Controller
{
    public function index()
    {
        $campaign = Campaign::paginate(2);
        $data['campaign'] = $campaign;

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'data campaing berhasil ditampilkan',
            'data' => $data,
        ], 200);
    }

    public function random($count)
    {
        $campaigns = Campaign::select('*')
            ->inRandomOrder()
            ->limit($count)
            ->get();

        $data['campaigns'] = $campaigns;

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'data campaing berhasil ditampilkan',
            'data' => $data,
        ], 200);
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png',
        ]);

        $campaign = Campaign::create([
            'title' => $request->title,
            'description' => $request->description,
        ]);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $image_extention = $image->getClientOriginalExtension();
            $image_name = $campaign->id . "." . $image_extention;
            $image_folder = '/photos/campaign/';
            $image_location = $image_folder . $image_name;

            try {
                $image->move(public_path($image_folder), $image_name);
                $campaign->update([
                    'image' => $image_location,
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'response_code' => '01',
                    'response_msg' => 'Foto profile gagal upload',
                    'data' => $data,
                ], 200);
            }
        }

        $data['campaign'] = $campaign;

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'data campaing berhasil ditambahkan',
            'data' => $data,
        ], 200);
    }

    public function detail($id){
        $campaign = Campaign::find($id);
        $data['campaign'] = $campaign;

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'data campaign berhasil ditambahkan',
            'data' => $data,
        ], 200);
    }

    public function search($keyword){
        $campaign = Campaign::select("*")
                             ->where('title','LIKE',"%".$keyword."%")
                             ->get();
        $data['campaign'] = $campaign;
        
        return response()->json([
            'response_code' => '00',
            'response_msg' => 'data campaign berhasil ditampilkan',
            'data' => $data,
        ], 200);
    }
}
