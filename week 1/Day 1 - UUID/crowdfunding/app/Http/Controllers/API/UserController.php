<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __constrcut(){
        $this->middleware('auth:api');
    }

    public function getProfile(){
        $user = Auth::user();

        $profile = [
            'profile' => $user,
        ];
        return response()->json([
            'response_code' => '00',
            'response_msg' => 'profile berhasil ditampilkan',
            'data' => $profile,
        ]);
    }

    public function updateProfile(Request $request){
        $user = Auth::user();

        $request->validate([
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name'  => 'required',
       ]);

        if($request->hasFile('photo')){
            $image = $request->file('photo');
            $image_extention = $image->getClientOriginalExtension();
            $image_name = $user->id.".".$image_extention;
            $image_folder = '/photos/profile/';
            $image_location = $image_folder.$image_name;
 
            try{
                $image->move(public_path($image_folder), $image_name);
                $user->name  = $request->name;
                $user->photo = $image_location;
                $user->save();
            }
            catch (\Exception $e){
             return response()->json([
                 'response_code' => '01',
                 'response_msg' => 'Foto profile gagal upload',
                 'data' => $data,
             ], 200);
            }
        }

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'profile berhasil ditampilkan',
            'data' => $user,
        ]);
    }
}
