<?php

namespace App\Http\Controllers\API;

use App\Models\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    public function random($count){
        $blogs = Blog::select('*')
                             ->inRandomOrder()
                             ->limit($count)
                             ->get();
         
         $data['blogs'] = $blogs;
 
         return response()->json([
             'response_code' => '00',
             'response_msg' => 'data blog berhasil ditampilkan',
             'data' => $data,
         ], 200);
    } 

    public function store(Request $request){
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png'
        ]);
 
        $Blog = Blog::create([
            'title' => $request->title,
            'description' => $request->description,
        ]);
 
        if($request->hasFile('image')){
            $image = $request->file('image');
            $image_extention = $image->getClientOriginalExtension();
            $image_name = $Blog->id.".".$image_extention;
            $image_folder = '/photos/blog/';
            $image_location = $image_folder.$image_name;
 
            try{
                $image->move(public_path($image_folder), $image_name);
                $Blog->update([
                    'image' => $image_location,
                ]);
            }
            catch (\Exception $e){
             return response()->json([
                 'response_code' => '01',
                 'response_msg' => 'Foto profile gagal upload',
                 'data' => $data,
             ], 200);
            }
        }
 
        $data['Blog'] = $Blog;
 
        return response()->json([
         'response_code' => '00',
         'response_msg' => 'data campaing berhasil ditambahkan',
         'data' => $data,
     ], 200);
    }
}
