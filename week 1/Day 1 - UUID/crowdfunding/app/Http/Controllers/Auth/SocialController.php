<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;

class SocialController extends Controller
{
    public function redirectToProvider($provider)
    {
        $url = Socialite::driver($provider)
            ->stateless()
            ->redirect()
            ->getTargetUrl();

        return response()->json([
            'url' => $url,
        ]);
    }

    public function handleProviderCallback($provider)
    {
        try {
            $social_user = Socialite::driver($provider)
                ->stateless()
                ->user();

            // dd($social_user);
            if (!$social_user) {
                return response()->json([
                    'response_code' => '01',
                    'response_msg'  => 'Account not found'
                ], 401);
            }
            $photo = '';
            $user = User::whereEmail($social_user->email)->first();
            // dd($user)
            if (!$user) {
                if($provider == "google"){
                    $photo = $social_user->avatar;
                }
                $user = User::create([
                    'name' => $social_user->name,
                    'email' => $social_user->email,
                ]);
                    
                $user->email_verified_at = Carbon::now();
                $user->photo = $photo;
                $user->save();
            }
            $data['user'] = $user;
            $data['token'] = auth()->login($user);
            // dd($data);
            return response()->json([
                'response_code' => '00',
                'response_msg'  => 'Login Success',
                'data'          => $data,
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'response_code' => '01',
                'response_msg'  => 'Login failed',
            ], 401);
        }
    }
}
