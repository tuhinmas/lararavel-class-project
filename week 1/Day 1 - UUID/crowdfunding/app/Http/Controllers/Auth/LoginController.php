<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
   public function login(Request $request)
   {
        $request->validate([
            'email' =>'required',
            'password' => 'required'
        ]);

        $user = User::where('email',$request->email)->first();

        if($user == null){
            return response()->json([
                'response_code' => '01',
                'response_msg' => 'email belum diverifikasi.'
            ]);
        }
        if (! $token = auth()->attempt($request->only('email','password'))) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->respondWithToken($token, $user);
   }
    protected function respondWithToken($token, $user)
    {
        $data = [
            'token' => $token,
            'user' => $user,
        ];
        return response()->json([
            'response_code' => "00",
            'response_msg' => 'Login berhasil',
            'data' => $data
        ]);
    }
}
