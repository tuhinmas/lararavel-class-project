<?php

namespace App\Http\Controllers\Auth;

use Mail;
use App\User;
use Carbon\Carbon;
use App\Models\OTP;
use App\Models\Role;
use App\Events\RegisterOtp;
use Illuminate\Http\Request;
use App\Http\Requests\OTPRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Mail\GenerateOtpUserRegistered;
use App\Http\Requests\RegenerateOTPRequest;
use App\Http\Requests\UpdatePasswordRequest;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegisterRequest $request)
    {
        $user  = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();

        $otp = OTP::firstOrCreate([
            'user_id' => $user->id,
            'otp' => rand(000000,999999),
            'valid_until' => Carbon::now()->addMinutes(5),
        ]);

        $otp_with_user = OTP::with('users')
                            ->where('user_id', $user->id)->first();

        event(new RegisterOtp($otp_with_user));

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'register berhasil',
            'data' => $user,
        ]);

    }

    public function verification(OTPRequest $request)
    {
        $otp = OTP::where('otp',$request->otp)
                  ->first();
        
        if($otp){
            if($otp->valid_until < Carbon::now()){
                return response()->json([
                    'response_code' => '01',
                    'response_msg' => 'kode otp tidak berlaku, silahkan generate ulang',
                ]);
            }

            $user = User::where('id',$otp->user_id)
                        ->first();

            $role = Role::where('role_name','user')->first();
            $user->email_verified_at = Carbon::now();
            $user->role_id = $role->id; 
            $user->save();
            
            return response()->json([
                'response_code' => '00',
                'response_msg' => 'verifikai berhasil',
            ]);
        }
        return response()->json([
            'response_code' => '01',
            'response_msg' => 'otp tidak ditemukan',
        ]);

    }

    public function regenerateOtp(RegenerateOTPRequest $request)
    {
        $user = User::where('email',$request->email)
                    ->first();

        if($user){ // apakah email terdaftar
            $otp = OTP::where('user_id',$user->id) //update otp jika ada
                      ->first();

            $otp->otp =rand(000000,999999);
            $otp->save();

            $otp_with_user = OTP::with('users')
                                ->where('user_id', $user->id)
                                ->first();
                            
            event(new RegisterOtp($otp_with_user));
        }

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'Silahkan buka email Anda',
            'data' => $user,
        ]);
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        $user = User::where('email',$request->email)->first();
        if($user){
            if($request->password == $request->password_confirmation){
                $user->password = bcrypt($request->password);
                $user->save();

                return response()->json([
                    'response_code' => '00',
                    'response_msg' => 'Password berhasil diperbarui',
                    'data' => $user,
                ]);
            }
            return response()->json([
                'response_code' => '01',
                'response_msg' => 'password harus sama',
            ]);
        }
        return response()->json([
            'response_code' => '01',
            'response_msg' => 'email tidak terdaftar',
        ]);
    }
    public function destroy($id)
    {
        //
    }
}
