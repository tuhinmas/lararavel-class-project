<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Auth;

class emailVerifiedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = Auth::id();
        $user = User::where('id',$id)
                    ->select('email_verified_at')
                    ->first();
        // dd($user->email_verified_at);
        if($user->email_verified_at != null){
            return $next($request);
        }
        abort(403);
    }
}
