<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;

class adminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(Auth::user());
        $id = Auth::id();
        $user = User::where('id',$id)
                    ->select('role_id')
                    ->first();
        $role_id = Role::where('id',$user->role_id)
                       ->select('role_name')
                       ->first();
        // dd($role_id->role_name);
        if($role_id->role_name == 'admin'){
            return $next($request);
        }
        abort(403);
    }
}
