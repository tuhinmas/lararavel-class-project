<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use App\Traits\UsesUuid;


class Role extends Model
{
    use UsesUuid;
    // protected static function boot(){
    //     parent::boot();
    //     static::creating(function($model){
    //         if(! $model->getKey()){
    //             $model->{$model->getKeyName()} = (string) Str::uuid();
    //         }
    //     });
    // }

    // public function getIncrementing(){
    //     return false;
    // }

    // public function getKeyType(){
    //     return 'string';
    // }

    public function users(){
        return $this->hasMany('/App/User','role_id','role_id');
    }
}
