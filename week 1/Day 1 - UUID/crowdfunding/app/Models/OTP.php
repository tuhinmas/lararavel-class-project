<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class OTP extends Model
{
    public $guarded =[];
    public $table = 'otp_codes';
    public $timestamps = false;
    protected static function boot(){
        parent::boot();
        static::creating(function($model){
            if(! $model->getKey()){
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
    }

    public function getIncrementing(){
        return false;
    }

    public function getKeyType(){
        return 'string';
    }

    public function users(){
        return $this->hasOne('App\User','id','user_id');
    }
}
