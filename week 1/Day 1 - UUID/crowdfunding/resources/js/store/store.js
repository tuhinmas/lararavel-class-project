import Vue from 'vue'
import Vuex from 'vuex'
import transaction from './transaction'
import alert from './alert.js'
import auth from './auth.js'
import dialog from './dialog.js'
import VuexPersist from 'vuex-persist'

const vuexPersist = new VuexPersist({
  key     : 'tuhinmas',
  storage : localStorage
})

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [vuexPersist.plugin],
  modules:{
    transaction,
    alert,
    dialog,
    auth,
  }
});
