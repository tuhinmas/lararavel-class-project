<?php
trait Hewan{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;
    public function atraksi(){
       return $this->nama." sedang ".$this->keahlian; 
    }
}

trait Fight{
    public $attackPower;
    public $defencePower;
    public function serang($diserang){
        return $this->nama."_".$this->darah." sedang menyerang ".$diserang->nama."_".$diserang->darah;
    }

    public function diserang($attacker){
        $this->darah = $this->darah - ($attacker->attackPower / $this->defencePower);
        return $this->nama." sedang diserang ";
    }
}

class Elang{
    use Hewan,Fight;
    public function __construct(){
        $this->nama = "Elang";
        $this->jumlahKali = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function getInfoHewan(){
        echo "<b>Data".$this->nama."</b><br>";
        echo "Nama :".$this->nama."<br>";
        echo "Jumlah kaki: ".$this->jumlahKaki."<br>"; 
        echo "Keahlian: ".$this->keahlian."<br>"; 
        echo "Attack Power: ".$this->attackPower."<br>"; 
        echo "Defence Power: ".$this->defencePower."<br>"; 
     }
}

class Harimau{
    use Hewan,Fight;
    public function __construct(){
        $this->nama = "Harimau";
        $this->jumlahKali = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getInfoHewan(){
        echo "<b>Data".$this->nama."</b><br>";
        echo "Nama :".$this->nama."<br>";
        echo "Jumlah kaki: ".$this->jumlahKaki."<br>"; 
        echo "Keahlian: ".$this->keahlian."<br>"; 
        echo "Attack Power: ".$this->attackPower."<br>"; 
        echo "Defence Power: ".$this->defencePower."<br>"; 
    }
}

$elang = new Elang();
$elang->getInfoHewan();
echo "<br>";
$harimau = new Harimau();
$harimau->getInfoHewan();

echo "<br>";
echo $elang->atraksi()."<br>";
echo $harimau->atraksi()."<br>";

echo "<br>";
echo "<b>Action</b>";
echo "<br>";
echo $elang->serang($harimau);
echo "<br>";
echo $harimau->serang($elang);
echo "<br><br>";

echo $elang->diserang($harimau)." sisa darah ".$elang->nama." ".$elang->darah;
echo "<br>";
echo $harimau->diserang($elang)." sisa darah ".$harimau->nama." ".$harimau->darah;
echo "<br>";
echo $elang->diserang($harimau)." sisa darah ".$elang->nama." ".$elang->darah;
echo "<br>";
echo $harimau->diserang($elang)." sisa darah ".$harimau->nama." ".$harimau->darah;
echo "<br>";
echo $elang->diserang($harimau)." sisa darah ".$elang->nama." ".$elang->darah;
echo "<br>";
echo $harimau->diserang($elang)." sisa darah ".$harimau->nama." ".$harimau->darah;
?>