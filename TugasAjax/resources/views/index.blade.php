<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div id="app">
        <div>
            <h3>
                Daftar User
            </h3>
            <input v-model="dataUser" type="text">
            <button @click="addOrUpdate? addUser() : updateUser()">@{{ buttonStatus }}</button>
        </div>
        <div v-show="show">
            <div v-for="(user, index) in users" style="padding-top:10px">
                <div>
                    @{{ user.name }}
                    <button @click="editUser(user)">Edit</button>
                    <button @click="hapusData(user.id)">Delete</button>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.12/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>

    <script>
        new Vue({
            el:'#app',
            data:{
                id:null,
                dataUser:'',
                buttonStatus:'Add',
                addOrUpdate:true,
                show:true,
                users:[],
            },
            methods:{
                addUser(){
                    this.$http.post('/api/v1/users',{
                            name: this.dataUser})
                        .then(res => {
                            console.log(res.data);
                        });
                    this.getData();
                    this.ShowingData();
                    this.dataUser = '';
                },
                getData(){
                    this.$http.get('/api/v1/users')
                          .then(res => {
                              this.users = res.data.data;
                    });
                },
                editUser(user){
                    this.dataUser = user.name;
                    this.buttonStatus = 'Update';
                    this.id = user.id;
                    this.addOrUpdate = false;
                },
                updateUser(){
                    this.$http.post('/api/v1/users/edit/'+this.id,{
                            name: this.dataUser
                            })
                        .then(res => {
                            console.log(res.data);
                        });
                    this.dataClearence();
                    this.ShowingData();
                },
                hapusData(userId){
                    this.$http.post('/api/v1/users/delete/'+userId)
                        .then(res => {
                            console.log(res.data);
                        });
                    this.dataClearence();
                    this.ShowingData();
                },
                dataClearence(){
                    this.getData();
                    this.addOrUpdate = true;
                    this.buttonStatus = 'Add';
                    this.dataUser = '';
                },
                ShowingData(){
                    this.show = false;
                    this.show = true;
                }
            },
            created(){
                // this.ShowingData();
                this.getData();
            }
        })
    </script>
</body>
</html>